package com.company;

import java.util.Scanner;

public class Main {

    static final int COUNT = 5;

    public static void main(String[] args) {
        int temp = 0;
        String[] strings = new String[COUNT];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < strings.length; i++) {
            strings[i] = scanner.nextLine();
            temp += strings.length;
        }
        for (String string : strings) {
            if (string.length() < (temp / strings.length)) {
                System.out.println(string);
            }
        }
    }
}
